import { StyleSheet, Dimensions } from 'react-native';
import { w, h } from '../../utils/Dimensions';
import { TextSize } from '../../theme/TextSize';
import { TextColor, UiColor } from '../../theme';
import fonts from '../../theme/fonts'

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  user: {
    height: h(22),
    // flex: 1,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: '#dcb141',
    padding:h(2)
  },
  empImage: {
    justifyContent: "flex-start",
    height: h(12),
    width: h(12),
    borderRadius: h(100),
    borderWidth: h(0.2),
    margin:h(1),
  },
  userDetail: {
    justifyContent: "flex-end",
   
  },
  userName: {
    color: '#000',
    fontSize: h(3),
    // fontFamily: fonts.lightText,left:5
  },
  userID: {
    color: '#000',
    fontSize: 16,
  },
  drawerList: {
    height: h(100),
    backgroundColor: '#fff',
    marginHorizontal: h(1)
  },
  inputIcon: {
    width: h(3),
    height: h(3),
    tintColor: "#dbb042",
    marginHorizontal: h(2),
  },
  drawerText: {
    fontSize: h(2.7),
    color: "#000",
    justifyContent:"center",
  },
  drawerView: {
    flexDirection: "row",
    marginVertical: h(2),
    alignItems: "center",
  },

});
