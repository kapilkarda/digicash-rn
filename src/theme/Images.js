
export default {
    splash: require('../assets/images/splash.png'),
    backgroundRedImage: require('../assets/assets/background.png'),
    digiCashLogo: require('../assets/assets/logo-02.png'),
    digiTextIconLogin: require('../assets/assets/cash_icon-02.png'),
    userIcon: require('../assets/assets/Newfolder/icon-02.png'),
    passwordLock: require('../assets/assets/Newfolder/icon-09.png'),
    passwordShowHide: require('../assets/assets/Newfolder/icon-04.png'),
    rightArrow: require('../assets/assets/Newfolder/icon-10.png'),
    uncheckedImage: require('../assets/assets/box.png'),
    checkedImage: require('../assets/assets/checkbox.png'),
  };