import React from 'react';
import { Scene, Router, Stack, Drawer } from 'react-native-router-flux';
import { Dimensions, Platform } from 'react-native';
import { connect } from "react-redux";

import Splash from './container/Splash/Splash';
import Login from './container/Login/Login';



var width = Dimensions.get('window').width;
const RouterWithRedux = connect()(Router);

class Root extends React.Component {

  render() {
    return (
      <RouterWithRedux>
        <Scene key="root" hideTabBar hideNavBar>
          <Stack key="app">
            <Scene hideNavBar panHandlers={null}>
              <Scene
                initial={true}
                component={Splash}
                hideNavBar={true}
                key="Splash"
                title="Splash"
                wrap={false}
              />
             <Scene
                component={Login}
                hideNavBar={true}
                key="Login"
                title="Login"
                wrap={false}
              />
              {/* <Drawer
                hideNavBar
                key="drawer"
                contentComponent={DrawerBar}
                wrap={false}
                drawerWidth={width - 100}>
                <Scene
                  component={Sites}
                  hideNavBar={true}
                  wrap={false}
                  key="Sites"
                  title="Sites"
                />
              </Drawer> */}
             
            </Scene>
          </Stack>
        </Scene>
      </RouterWithRedux>
    );
  }
}

export default Root;
console.disableYellowBox = true;
