import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings({
    en: {
        Login: {
            welcomeText:
                'WELCOME TO',
            forgotText:
                'Forgot Password?',
            rememberMe:
                'Remember Me'
        },
    }
});

export default strings;