import { combineReducers } from 'redux';
import Login from "./Login";
import DrawerMenuReducer from "./DrawerMenuReducer";
import CheckOfflineReducer from './CheckOfflineReducer';
import SideScreenReducer from './SideScreenReducer';
import ForgetPassReducer from './ForgetPassReducer';
import IndChapterReducer from './IndChapterReducer';
import TrainingReducer  from './TrainingReducer';
import QualityInductionReducer from './QualityInductionReducer';
import QualityTrainingReducer from './QualityTraining';
import ActionListReducer from './ActionListReducer';
import UploadImageReducer from './UploadImageReducer';
import AuditdetailsReducer from './AuditdetailsReducer';
import IncidentdetailsReducer from './IncidentdetailsReducer';
import CreateActionReducer from './CreateActionReducer';
import ActionPlanReducer from './ActionPlanReducer';
import ActionPlanDeleteReducer from './ActionPlanDeleteReducer';
import UpdateAuditFormReducer from './UpdateAuditFormReducer';
import UpdateIncidentFormReducer from './UpdateIncidentFormReducer';
import SignatureUploadReducer from './SignatureUploadReducer';
import AuditScheduleReducer from './AuditScheduleReducer';
import ActionPlanByIdReducer from './ActionPlanByIdReducer';
import UpdateScheduleStatusReducer from './UpdateScheduleStatusReducer';
import EditActionPlanReducer from './EditActionPlanReducer';

export default combineReducers({
    login: Login,
    checkOfflineFeature:CheckOfflineReducer,
    drawermenu:DrawerMenuReducer,
    forgetpaswword:ForgetPassReducer,
    sidescreen:SideScreenReducer,
    inductionchapter:IndChapterReducer,
    trainingReducer: TrainingReducer,
    QualityInduction:QualityInductionReducer,
    QualityTraining:QualityTrainingReducer,
    ActionList:ActionListReducer,
    UploadImage:UploadImageReducer,
    Auditdetails:AuditdetailsReducer,
    Incidentdetails:IncidentdetailsReducer,
    createAction:CreateActionReducer,
    actionplanview:ActionPlanReducer,
    ActionDelte:ActionPlanDeleteReducer,
    UpdateAuditFormReducer:UpdateAuditFormReducer,
    UpdateIncidentFormReducer:UpdateIncidentFormReducer,
    SignatureUploadReducer:SignatureUploadReducer,
    AuditScheduleReducer:AuditScheduleReducer,
    ActionPlanByIdReducer:ActionPlanByIdReducer,
    UpdateScheduleStatus: UpdateScheduleStatusReducer,
    EditActionPlanReducer: EditActionPlanReducer,
})