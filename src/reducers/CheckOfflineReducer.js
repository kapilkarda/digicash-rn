import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    checkOfflineFeature: true,
};

export default (state = initialState, action) => {
  console.log("offline reducer",action)
  const { type } = action;

  switch (type) {
    case ActionTypes.CHECK_OFFLINE:
      console.log("CHECK_OFFLINE reducer",type)
      return action.data;
    default:
      return state;
  }
};