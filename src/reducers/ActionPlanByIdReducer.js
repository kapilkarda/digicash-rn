import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    isactionplanbyId: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.ACTIONPLANBYID:
            return action.data;
        default:
            return state
    }
}