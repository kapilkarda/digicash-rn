import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    isAuditSchedule: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.SCHEDULELIST:
            return action.data;
        default:
            return state
    }
}