import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    isincidentdetails: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.INCIDENT_DETAILS:
            return action.data;
        default:
            return state
    }
}