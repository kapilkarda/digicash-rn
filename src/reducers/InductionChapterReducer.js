import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    isInductionChapter: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.INDUCTIONCHAPTERS:
            return action.data;
        default:
            return state
    }
}