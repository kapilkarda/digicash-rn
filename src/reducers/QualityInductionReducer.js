import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    isqualityinDuction: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.QUALITYINDUCTION:
            return action.data;
        default:
            return state
    }
}