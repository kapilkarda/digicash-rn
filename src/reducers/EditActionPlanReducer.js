import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    iseditactionplan: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.EDITACTIONPLAN:
            return action.data;
        default:
            return state
    }
}