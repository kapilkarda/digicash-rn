import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    isactionplanview: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.ACTIONPLANVIEW:
            return action.data;
        default:
            return state
    }
}