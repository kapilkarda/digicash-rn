import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    isinductionchapter: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.INDUCTIONCHAPTER:
            return action.data;
        default:
            return state
    }
}