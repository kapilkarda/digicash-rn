import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    isUpdateAuditForm: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.UPDATEAUDITFORM:
            return action.data;
        default:
            return state
    }
}