import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    isactionlist: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.ACTIONLIST:
            return action.data;
        default:
            return state
    }
}