import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    iscreateaction: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.CREATEACTION:
            return action.data;
        default:
            return state
    }
}