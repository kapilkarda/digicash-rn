import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    isUpdateSchedule: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.UPDATESCHEDULESTATUS:
            return action.data;
        default:
            return state
    }
}