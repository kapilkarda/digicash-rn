import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    isactionplandelete: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.ACTIONPLANDELETE:
            return action.data;
        default:
            return state
    }
}