import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    isUpdateIncidentForm: false,
    hasError: false,
    isLoading: false,
};

export default (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case ActionTypes.UPDATEINCIDENTFORM:
            return action.data;
        default:
            return state
    }
}