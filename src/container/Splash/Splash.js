import React from 'react';
import {View, Animated, StatusBar, Image} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import styles from './styles';
import Images from '../../theme/Images'
import AsyncStorage from '@react-native-community/async-storage';

class Splash extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fadeValue: new Animated.Value(0),
      user_id: '',
    };
  }

  componentDidMount = async () => {
    try {
      setTimeout(async () => {
        Actions.Login();
      }, 1500);
    } catch (error) {
      console.log('error' + error);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar />
        <Image resizeMode="cover" source={Images.splash} style={styles.splashImg} />
      </View>
    );
  }
}

export default connect(null, null)(Splash);
