import { StyleSheet } from 'react-native';
import { w, h } from '../../utils/Dimensions';
import fonts from '../../theme/fonts'
export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor:'#E9414C'
  },
  loginBackgroundImageStyle: {
    height:'100%', 
    width:'100%'
  },
  welcomeViewStyle: {
    flex:2.3, 
    alignItems:'center', 
    justifyContent:'center',
    marginBottom:10
  },
  welcomeHeadingTextStyle: {
    color:'#fff', 
    fontSize:20, 
    fontWeight:'600', 
    marginTop:h(7)
  },
  digiCashLogoStyle: {
    height:h(17), 
    width:h(20), 
    marginTop:h(2.5)
  },
  letsgetSomeMainView: {
    flexDirection:'row'
  },
  letsgetTextStyle: {
    color:'#f2f2f2', 
    fontSize:21, 
    marginTop:2,
    alignSelf:'center'
  },
  digiCashLogo:{
    height:h(4.5), 
    width:w(22)
  },
  loginContentMainView:{
    flex: 4,
    backgroundColor: '#fff',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  }
});
