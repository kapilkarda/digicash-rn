import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {
  View,
  ScrollView,
  Text,
  Image,
  TextInput,
  Alert,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback,
  SafeAreaView,
  ImageBackground,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import styles from './styles';
import {w, h} from '../../utils/Dimensions';
import {TextSize} from '../../theme/TextSize';
import Loader from '../../constants/Loader';
import Images from '../../theme/Images';
import AsyncStorage from '@react-native-community/async-storage';
import CheckBox from 'react-native-check-box';

class Login extends React.Component {
  constructor(props) {
    super(props);
    sitesData = true;
    this.state = {
      password: '',
    };
  }

  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <ImageBackground
            source={Images.backgroundRedImage}
            style={styles.loginBackgroundImageStyle}>
            <View style={styles.welcomeViewStyle}>
              <Text style={styles.welcomeHeadingTextStyle}>WELCOME TO</Text>
              <Image
                source={Images.digiCashLogo}
                style={styles.digiCashLogoStyle}
              />
              <View style={styles.letsgetSomeMainView}>
                <Text style={styles.letsgetTextStyle}>Let's get some</Text>
                <Image
                  resizeMode="contain"
                  source={Images.digiTextIconLogin}
                  style={styles.digiCashLogo}
                />
              </View>
            </View>
            <View style={styles.loginContentMainView}>
              <View style={{paddingHorizontal: h(2), paddingTop: 10}}>
                <Text
                  style={{
                    color: 'grey',
                    paddingVertical: h(1.5),
                    fontSize: 13,
                  }}>
                  DigiID, E-mail, Or Mobile Number
                </Text>
                <View
                  style={{
                    width: '100%',
                    backgroundColor: '#f2f2f2',
                    height: h(7),
                    borderRadius: 50,
                    flexDirection: 'row',
                  }}>
                  <View
                    style={{
                      width: '13%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Image
                      source={Images.userIcon}
                      style={{height: h(4), width: h(4), resizeMode: 'contain'}}
                    />
                  </View>
                  <TextInput
                    style={{width: '85%', borderRadius: 50, fontSize: 16}}
                    placeholder="William_smith02"
                    placeholderTextColor="grey"
                  />
                </View>
              </View>
              <View style={{paddingHorizontal: h(2)}}>
                <Text
                  style={{
                    color: 'grey',
                    paddingVertical: h(1.5),
                    fontSize: 13,
                  }}>
                  Password
                </Text>
                <View
                  style={{
                    width: '100%',
                    backgroundColor: '#f2f2f2',
                    height: h(7),
                    borderRadius: 50,
                    flexDirection: 'row',
                  }}>
                  <View
                    style={{
                      width: '13%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Image
                      source={Images.passwordLock}
                      style={{height: h(4), width: h(4), resizeMode: 'contain'}}
                    />
                  </View>
                  <TextInput
                    style={{
                      width: '73%',
                      borderRadius: 50,
                      fontSize: 16,
                      marginTop: this.state.password == '' ? 7 : 0,
                    }}
                    placeholder="***********"
                    placeholderTextColor="grey"
                    onChangeText={(text) => this.setState({password: text})}
                  />
                  <View
                    style={{
                      width: '13%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Image
                      source={Images.passwordShowHide}
                      style={{height: h(4), width: h(4), resizeMode: 'contain'}}
                    />
                  </View>
                </View>
                <TouchableOpacity
                  style={{
                    backgroundColor: '#D53546',
                    width: '100%',
                    height: h(7),
                    alignSelf: 'center',
                    marginTop: h(3),
                    borderRadius: 50,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingHorizontal: 5,
                  }}>
                  <View style={{width: w(10)}} />
                  <View style={{alignSelf: 'center'}}>
                    <Text
                      style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
                      Sign in
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: '#fff',
                      width: h(5.3),
                      height: h(5.3),
                      borderRadius: h(6),
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Image
                      source={Images.rightArrow}
                      style={{width: h(5), height: h(5)}}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: h(2.5),
                  paddingVertical: 10,
                  marginTop: 10,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <CheckBox
                    style={{marginTop: 2}}
                    onClick={() => {
                      this.setState({
                        isChecked: !this.state.isChecked,
                      });
                    }}
                    isChecked={this.state.isChecked}
                    checkedImage={
                      <Image
                        style={{width: h(2.5), height: h(2.5)}}
                        source={Images.checkedImage}
                      />
                    }
                    unCheckedImage={
                      <Image
                        style={{
                          width: h(2.5),
                          height: h(2.5),
                          tintColor: 'lightgrey',
                        }}
                        source={Images.uncheckedImage}
                      />
                    }
                  />
                  <Text
                    style={{
                      marginLeft: 10,
                      alignSelf: 'center',
                      color: 'grey',
                    }}>
                    Remember Me
                  </Text>
                </View>
                <TouchableOpacity>
                  <Text style={{color: 'grey', alignSelf: 'center'}}>
                    Forgot Password?
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: '90%',
                  alignSelf: 'center',
                  height: 1,
                  backgroundColor: 'grey',
                }}
              />
              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  paddingTop: h(2),
                }}>
                <Text style={{color: 'grey'}}>Don't have an account?</Text>
                <TouchableOpacity>
                  <Text style={{color: '#E9414C', marginLeft: 3}}>Sign up</Text>
                </TouchableOpacity>
              </View>
              <Text
                style={{
                  paddingTop: h(1.5),
                  textAlign: 'center',
                  color: '#000',
                }}>
                Or
              </Text>
              <Text
                style={{
                  paddingTop: h(1.5),
                  textAlign: 'center',
                  color: 'grey',
                }}>
                Connect with
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  paddingTop: h(2),
                }}>
                <TouchableOpacity>
                  <Image
                    source={require('../../assets/assets/Newfolder/icon-11.png')}
                    style={{height: h(6.5), width: h(6.5)}}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../../assets/assets/Newfolder/icon-12.png')}
                    style={{height: h(6.5), width: h(6.5), marginLeft: w(2)}}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../../assets/assets/Newfolder/icon-13.png')}
                    style={{height: h(6.5), width: h(6.5), marginLeft: w(2)}}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../../assets/assets/Newfolder/icon-14.png')}
                    style={{height: h(6.5), width: h(6.5), marginLeft: w(2)}}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../../assets/assets/Newfolder/icon-15.png')}
                    style={{height: h(6.5), width: h(6.5), marginLeft: w(2)}}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../../assets/assets/Newfolder/icon-16.png')}
                    style={{height: h(6.5), width: h(6.5), marginLeft: w(2)}}
                  />
                </TouchableOpacity>
              </View>
              <View style={{paddingTop: h(3)}}>
                <Text
                  style={{fontSize: 13, textAlign: 'center', paddingTop: h(1)}}>
                  By using this app, you agree to our{' '}
                  <Text style={{color: '#E9414C'}}>Terms of Use</Text> and{' '}
                </Text>
                <TouchableOpacity>
                  <Text
                    style={{
                      color: '#E9414C',
                      paddingTop: h(1),
                      textAlign: 'center',
                      fontSize: 13,
                    }}>
                    Privacy Policy.
                  </Text>
                </TouchableOpacity>
              </View>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 13,
                  paddingTop: h(1),
                  paddingBottom: h(2),
                }}>
                Powered by RMEI Designs
              </Text>
            </View>
          </ImageBackground>
        </ScrollView>
      </TouchableWithoutFeedback>
    );
  }
}

Login.propTypes = {};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
