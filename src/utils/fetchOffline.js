import { AsyncStorage } from "react-native";
import SQLite from "react-native-sqlite-storage";
import NetInfo from "@react-native-community/netinfo";

const db = SQLite.openDatabase("Masscon.db");

export function cacheReponse(url, options, responseBody, tableName) {
    console.log("cacheReponse responseBody",responseBody);
	const responseBodyJSON = JSON.stringify(responseBody);

	if (options.method === "GET") {
		db.transaction(tx => {
			tx.executeSql(
				`drop table if exists ${tableName};`,
				[],
				() => {},
				() => {}
			);
			tx.executeSql(
				`create table if not exists ${tableName}(id integer primary key not null, response text);`,
				[],
				() => {},
				() => {}
			);
			tx.executeSql(`insert into ${tableName} (response) values (?) `, [responseBodyJSON]);
		});
	}
}

export function storePostRequest(url, options, tableName) {
	const stringifyOptions = JSON.stringify(options);
	const syncTable = "syncTable";
	db.transaction(tx => {
		tx.executeSql(
			`create table if not exists ${syncTable}(id integer primary key not null,updatePageName text,apiUrl text,options text,response text,request text);`,
			[]
		);
		tx.executeSql(
			`insert into ${syncTable} (updatePageName,apiUrl,options,response,request) values (?,?,?,?,?) `,
			[tableName, url, stringifyOptions, "", options.body],
			() => {}
		);
	});
}

const fetchPromise = (url, options, tableName) => {
	return new Promise((resolve, reject) => {
		fetch(url, options)
			.then(response => response.json())
			.then(response => {
                console.log("fetchPromise response",response);
				cacheReponse(url, options, response, tableName);
				resolve(response);
			})
			.catch(error => {
				reject(error);
			});
	});
};

export async function getFailCaseResponse(url, options, tableName) {
    console.log("data getFailCaseResponse",tableName)
	return new Promise(resolve => {
        console.log("before")
		db.transaction(tx => {
			tx.executeSql(`select * from ${tableName}`, [], (_, { rows }) => {
                // eslint-disable-next-line no-underscore-dangle
                console.log("rows",rows);
                const data = JSON.parse(rows._array[0].response);
                console.log("data getFailCaseResponse",data)
				data.fallbackResponse = true;
				resolve(data);
			});
		});
	});
}

async function getDataFromCache(url, options, tableName) {
    console.log("getDataFromCache url",url);
    const data = await getFailCaseResponse(url, options, tableName);
    console.log("data getDataFromCache",data);
	if (data) {
		return new Promise(resolve => {
			resolve(data);
		});
	}
	return Promise.reject(new Error("No Internet"));
}

export default async function fetchOffline(url, options, tableName) {
	// const isConnected = await NetInfo.isConnected.fetch();
	const isReachable = await NetInfo.fetch();
	if (isReachable && isReachable.type !== "none") {
        console.log("inside if is server reachable");
		const promise = await fetchPromise(url, options, tableName);
		return promise;
	}
	// return await getStoredData(url, options, tableName)
	// if (
	// 	// eslint-disable-next-line no-prototype-builtins
	// 	(options && options.hasOwnProperty("method") && options.method.toLowerCase() === "post") ||
	// 	options.method.toLowerCase() === "put"
	// ) {
	// 	storePostRequest(url, options, tableName);
	// 	return null;
	// }
	return getDataFromCache(url, options, tableName);
}

export const runOfflineRequests = async callback => {
	const offlineRequests = await AsyncStorage.getItem("offlineRequests");
	if (offlineRequests !== null) {
		const requests = JSON.parse(offlineRequests);
		const allPromises = requests.map(req => {
			const { url, options } = req;
			return fetchPromise(url, options);
		});
		await Promise.all(allPromises)
			// eslint-disable-next-line no-unused-vars
			.then(async results => {
				await AsyncStorage.removeItem("offlineRequests");
				callback();
			})
			// eslint-disable-next-line no-unused-vars
			.catch(async error => {
				await AsyncStorage.removeItem("offlineRequests");
				callback();
			});
	} else {
		callback();
	}
};

export const getStoredData = async (url, options, tableName) => {
	return db.transaction(tx => {
		return tx.executeSql(`select * from ${tableName}`, [], (_, { rows }) => {
			// eslint-disable-next-line no-underscore-dangle
			return JSON.parse(rows._array[0].response);
		});
	});
};