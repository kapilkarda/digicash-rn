import * as ActionTypes from '../constants/ActionTypes';
import { BaseUrl } from '../constants/api';
import {Alert, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import AsyncStorage from '@react-native-community/async-storage';
export function inductionchapterRes(data) {
    return {
        type: ActionTypes.INDUCTIONCHAPTER,
        data
    }
};
export function indchapterAPI(data) {
 console.log("listInductionChapter",data)
 console.log("listInductiontoken",data.token)
 return (dispatch) => {
 fetch(BaseUrl + `/listInductionChapter`, {
method: 'POST',
headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         'Authorization' : data.token
         },
body: JSON.stringify(data)
})
.then((res) => res.json())
.then(res => {
console.log("listInductionChapterRes",res)

if (res.success === true) {
   
    dispatch(inductionchapterRes(res));
} else  {
    dispatch(inductionchapterRes(res));
}
})
.catch((e) => {
console.warn(e);
});
}
};

export function trainingRes(data) {
    return {
        type: ActionTypes.TRAINING,
        data
    }
};
  export function traningAPI(data) {
  console.log("traningAPI",data)
  console.log("listInductiontoken",data.token)
  return (dispatch) => {
  fetch(BaseUrl + `/listContent`, {
  method: 'POST',
  headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         'Authorization' : data.token
         },
  body: JSON.stringify(data)
  })
 .then((res) => res.json())
 .then(res => {
  console.log("listInductionChapter",res)

  if (res.success === true) {
   
    dispatch(trainingRes(res));
} else  {
    dispatch(trainingRes(res));
}
})
.catch((e) => {
console.warn(e);
});
}
};


export function qualityInductionRes(data) {
    return {
        type: ActionTypes.QUALITYINDUCTION,
        data
    }
};
  export function qualityInductionAPI(data) {
  console.log("qualityInductionAPI",data)
  console.log("listInductiontoken",data.token)
  return (dispatch) => {
  fetch(BaseUrl + `/inductionChapterById`, {
  method: 'POST',
  headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         'Authorization' : data.token
         },
  body: JSON.stringify(data)
  })
 .then((res) => res.json())
 .then(res => {
  console.log("qualityInductionAPI",res)

  if (res.success === true) {
   
    dispatch(qualityInductionRes(res));
} else  {
    dispatch(qualityInductionRes(res));
}
})
.catch((e) => {
console.warn(e);
});
}
};
 
export function qualitytrainingRes(data) {
    return {
        type: ActionTypes.QUALITYTRAINING,
        data
    }
};
export function qualitytrainingAPI(data) {
    console.log("qualityInductionAPI",data)
    console.log("listInductiontoken",data.token)
    return (dispatch) => {
    fetch(BaseUrl + `/contentById`, {
    method: 'POST',
    headers: {
           'Accept': 'application/json',
           'Content-Type': 'application/json',
           'Authorization' : data.token
           },
    body: JSON.stringify(data)
    })
   .then((res) => res.json())
   .then(res => {
    console.log("contentById",res)
  
    if (res.success === true) {
     
      dispatch(qualitytrainingRes(res));
  } else  {
      dispatch(qualitytrainingRes(res));
  }
  })
  .catch((e) => {
  console.warn(e);
  });
  }
  };
  