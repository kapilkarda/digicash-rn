import * as ActionTypes from '../constants/ActionTypes';
import { BaseUrl } from '../constants/api';
import {Alert, AsyncStorage, Platform} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import {Alert} from 'react-native';

export function uploadImageRes(data) {
    return {
        type: ActionTypes.UPLOAD_IMAGE,
        data
    }
};
export function uploadImageApi(data) {
    console.log('uploadImageApisw1',data)
    var getFilename = data.uri.split("/");
    var imgName = getFilename[getFilename.length - 1];
    console.log("imgName",imgName);
    var imageData = new FormData();
    imageData.append('photo', {
        uri: data.uri,
        type: data.type,
        name: Platform.OS == 'ios' ? imgName : data.fileName
    });  
     console.log("imageData",imageData)
    return async (dispatch) => {
        await fetch(`http://54.201.160.69:3022/uploadImage`, {
            method: 'POST',
            body: imageData,
             headers: {
                'Content-Type': 'multipart/form-data',
            },
        })
            .then((res) => res.json())
            .then(res => {
                console.log('res',res)
                if (res.success === "True")  {
                    dispatch(uploadImageRes(res));
                } else {
                    Alert.alert(res.message);
                    dispatch(uploadImageRes(res));
                }
            })
            .catch((e) => {
                console.log("error",e);
            });
    }
};


export function createActionRes(data) {
    return {
        type: ActionTypes.CREATEACTION,
        data
    }
};

export function craeteactionAPI(data) {
    console.log("craeteactionAPI",data)
    console.log("tokenapi",data.token)
    return (dispatch) => {
        fetch(BaseUrl + `/addActionPlan`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization' : data.token
            },
            body: JSON.stringify(data)
        })
            .then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.status === "success") {
                    dispatch(createActionRes(res));
                //   Actions.Login()
            } else {
                    dispatch(createActionRes(res));
                }
            })
            .catch((e) => {
                //  console.warn(e);
            });
    }
};

