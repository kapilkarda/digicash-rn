import * as ActionTypes from '../constants/ActionTypes';

export function checkOfflineRes(data) {
    console.log("checkOfflineRes data",data);
    return {
        type: ActionTypes.CHECK_OFFLINE,
        data
    }
};