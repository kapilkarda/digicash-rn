import * as ActionTypes from '../constants/ActionTypes';
import { BaseUrl } from '../constants/api';
import {Alert, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import {Alert} from 'react-native';

export function SignatureuploadImageRes(data) {
    return {
        type: ActionTypes.SIG_UPLOAD_IMAGE,
        data
    }
};
export function SignatureImageApi(data) {
    console.log('SignatureuploadImageRes',data)
    const sendReq = {
        "base64" : data.uri
    }
    // var imageData = new FormData();
    // imageData.append('asdfa', {
    //     uri:  data.uri,
    //     type: data.type,
    //     name: data.name
    // });  
    //  console.log("imageData",imageData)
    return async (dispatch) => {
        await fetch(`http://54.201.160.69:3022/imageUploadB64`, {
            method: 'POST',
            body: JSON.stringify(sendReq),
             headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((res) => res.json())
            .then(res => {
                console.log('res',res)
                if (res.success === "True")  {
                    dispatch(SignatureuploadImageRes(res));
                } else {
                    Alert.alert(res.message);
                    dispatch(SignatureuploadImageRes(res));
                }
            })
            .catch((e) => {
                console.log("error",e);
            });
    }
};

