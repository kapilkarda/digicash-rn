import * as ActionTypes from '../constants/ActionTypes';
import { BaseUrl } from '../constants/api';
import {Alert, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';
export function actionplanviewtRes(data) {
    return {
        type: ActionTypes.ACTIONPLANVIEW,
        data
    }
};

export function actionplanAPI(data) {
    console.log("actionplanAPI",data)
    return (dispatch) => {
        fetch(BaseUrl + `/actionPlanById`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization' : data.token
            },
            body: JSON.stringify(data)
        })
            .then((res) => res.json())
            .then(res => {
                console.log("actionlistAPI",res)
                if (res.status === "success") {
                    dispatch(actionplanviewtRes(res));
                   
                } else {
                   
                    dispatch(actionplanviewtRes(res));
                }
            })
            .catch((e) => {
                //  console.warn(e);
            });
    }
};


export function actionplandeleteRes(data) {
    return {
    type: ActionTypes.ACTIONPLANDELETE,
    data
    }
    };
    
    export function actionplandeleteAPI(data) {
    console.log("actionplanAPI",data)
    return (dispatch) => {
    fetch(BaseUrl + `/deleteActionPlan`, {
    method: 'POST',
    headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization' : data.token
    
    },
    body: JSON.stringify(data)
    })
    .then((res) => res.json())
    .then(res => {
    console.log("actionPlanDeleteAPI",res)
    if (res.status === "success") {
    dispatch(actionplandeleteRes(res));
   
    
    } else {
    
    dispatch(actionplandeleteRes(res));
    }
    })
    .catch((e) => {
    // console.warn(e);
    });
    }
    };



