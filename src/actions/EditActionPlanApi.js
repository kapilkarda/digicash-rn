import * as ActionTypes from '../constants/ActionTypes';
import {BaseUrl} from '../constants/api';
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';

export function EditActionPlanRes(data) {
  return {
    type: ActionTypes.EDITACTIONPLAN,
    data,
  };
}

export function editActionPlanApi(value) {
  return (dispatch) => {
    fetch(BaseUrl + `/editActionPlan`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': value.token,
      },
      body: JSON.stringify(value.data[0]),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log("editActionPlanApiresponse",res);
        if (res.status === 'success') {
          dispatch(EditActionPlanRes(res));
        } else {
          dispatch(EditActionPlanRes(res));
        }
      })
      .catch((e) => {
        console.log("e", e);
      });
  };
}
