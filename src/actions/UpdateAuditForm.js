import * as ActionTypes from '../constants/ActionTypes';
import { BaseUrl } from '../constants/api';
import {Alert, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';

export function UpdateAuditFormRes(data) {
    return {
        type: ActionTypes.UPDATEAUDITFORM,
        data
    }
};
export function UpdateAuditForm(data) {
  console.log("sidescreenAPI",data)
  return (dispatch) => {
  fetch(BaseUrl + `/updateAuditForm`, {
  method: 'POST',
  headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization':data.token
            },
  body: JSON.stringify(data)
})
  .then((res) => res.json())
  .then(res => {
  console.log("updateAuditForm",res)
  dispatch(UpdateAuditFormRes(res));
})
.catch((e) => {
                //  console.warn(e);
});
}
};

