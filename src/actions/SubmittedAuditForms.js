import * as ActionTypes from '../constants/ActionTypes';
import { BaseUrl } from '../constants/api';
import {Alert, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';

export function SubmittedAuditFormsRes(data) {
    return {
        type: ActionTypes.SUBMITAUDITFORMS,
        data
    }
};
export function SubmittedAuditForms(data) {
  console.log("SubmittedAuditForms",data);
  const value = {
    uid: data.uid
  }
  console.log("value",value);
  return (dispatch) => {
  fetch(BaseUrl + `/auditFormByFormId`, {
  method: 'POST',
  headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization':data.token
            },
  body: JSON.stringify(value)
})
  .then((res) => res.json())
  .then(res => {
  console.log("SubmittedAuditFormsRes",res)
  dispatch(SubmittedAuditFormsRes(res));
})
.catch((e) => {
                //  console.warn(e);
});
}
};

