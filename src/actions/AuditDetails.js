import * as ActionTypes from '../constants/ActionTypes';
import {BaseUrl} from '../constants/api';
import { Alert } from 'react-native';
import {Actions} from 'react-native-router-flux';

export function AuditDetailsRes(data) {
  return {
    type: ActionTypes.AUDIT_DETAILS,
    data,
  };
}

export function auditdetailsApi(data) {
  console.log('auditdetailsApi', data);
  return (dispatch) => {
    fetch(BaseUrl + `/auditFormById`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: data.token,
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        if (res.status === 'success') {
          dispatch(AuditDetailsRes(res));
        } else {
          dispatch(AuditDetailsRes(res));
        }
      })
      .catch((e) => {});
  };
}
