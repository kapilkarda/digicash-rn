import * as ActionTypes from '../constants/ActionTypes';
import { BaseUrl } from '../constants/api';
import {Alert} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
export function drawerRes(data) {
    return {
        type: ActionTypes.DRAWERMENU,
        data
    }
};

export function resetDrawerRes(data) {
    return {
        type: ActionTypes.SIDESCREEN,
        data
    }
};

 export  function drawermenuAPI(data) {
  return (dispatch) => {
  fetch(BaseUrl + `/logout`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                
            },
            body: JSON.stringify(data)
        })
            .then((res) => res.json())
            .then(res => {
            if (res.status === "success" ) {
            dispatch(drawerRes(res));
             AsyncStorage.removeItem('user_id')
            } else {
            // Alert.alert(res.message);
            dispatch(drawerRes(res));
                }
            })
            .catch((e) => {
                //  console.warn(e);
            });
    }
};

