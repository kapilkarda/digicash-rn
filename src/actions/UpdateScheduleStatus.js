import * as ActionTypes from '../constants/ActionTypes';
import {BaseUrl} from '../constants/api';
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';

export function UpdateScheduleRes(data) {
  return {
    type: ActionTypes.UPDATESCHEDULESTATUS,
    data,
  };
}
export function UpdateScheduleApi(data) {
  console.log('UpdateScheduleRes', data);
  const sendData = {
      id: data.id,
      fromDate: data.fromDate
  }
  return (dispatch) => {
    fetch(BaseUrl + `/updateScheduleStatus`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': data.token,
      },
      body: JSON.stringify(sendData),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('UpdateScheduleRes', res);
        dispatch(UpdateScheduleRes(res));
      })
      .catch((e) => {
        //  console.warn(e);
      });
  };
}
