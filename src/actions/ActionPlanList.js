import * as ActionTypes from '../constants/ActionTypes';
import { BaseUrl } from '../constants/api';
import {Alert, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';
import fetchOffLine from "../database";
export function actionlistRes(data) {
    return {
        type: ActionTypes.ACTIONLIST,
        data
    }
};

// export function actionlistAPI(data) {
//     console.log("actionlistAPI",data)
//     return (dispatch) => {
//         fetch(BaseUrl + `/listActionPlanByUserId`, {
//             method: 'POST',
//             headers: {
//                 'Accept': 'application/json',
//                 'Content-Type': 'application/json',
//                 'Authorization' : data.token

//             },
//             body: JSON.stringify(data)
//         })
//             .then((res) => res.json())
//             .then(res => {
//                 console.log("actionlistAPI",res)
//                 if (res.status === "success") {
//                     dispatch(actionlistRes(res));
                   
//                 } else {
                   
//                     dispatch(actionlistRes(res));
//                 }
//             })
//             .catch((e) => {
//                 //  console.warn(e);
//             });
//     }
// };

export const actionlistAPI = data => {
	return async dispatch => {
		await fetchOffLine(
			BaseUrl + `/listActionPlanByUserId`,
			{
				method: "POST",
				headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization' : data.token
    
                },
                body: JSON.stringify(data),
				offline: {
					defaultResponse: []
				}
			},
			"action_list_table"
		)
			.then(res => {
				if (res.status === "success") {
                    dispatch(actionlistRes(res));
                } else {    
                    dispatch(actionlistRes(res));
                }
			})
			.catch(error => {
			});
	};
};
