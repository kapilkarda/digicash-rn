import * as ActionTypes from '../constants/ActionTypes';
import {BaseUrl} from '../constants/api';
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';

export function AuditScheduleRes(data) {
  return {
    type: ActionTypes.SCHEDULELIST,
    data,
  };
}

export function auditScheduleApi(data) {
  console.log('auditScheduleApi', data);
  const sendRequest = {
    "siteId": data.siteId,
    "frequency": data.frequency
  }
  return (dispatch) => {
    fetch(BaseUrl + `/scheduleBySiteIdandFrequency`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': data.token,
      },
      body: JSON.stringify(sendRequest),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log("auditScheduleApiresponse",res);
        if (res.status === 'success') {
          dispatch(AuditScheduleRes(res));
        } else {
          dispatch(AuditScheduleRes(res));
        }
      })
      .catch((e) => {
        console.log("e", e);
      });
  };
}
