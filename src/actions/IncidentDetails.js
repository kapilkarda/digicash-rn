import * as ActionTypes from '../constants/ActionTypes';
import { BaseUrl } from '../constants/api';
import {Alert, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import {Alert} from 'react-native';

export function IncidentDetailsRes(data) {
    return {
        type: ActionTypes.INCIDENT_DETAILS,
        data
    }
};
export function incidentdetailsApi(data) {
    console.log('incidentFormById',data)
   return (dispatch) => {
    fetch(BaseUrl + `/incidentFormById`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
             'Content-Type': 'application/json',
             'Authorization' : data.token
        },
        body: JSON.stringify(data)
        })
        .then((res) => res.json())
        .then(res => {
        console.log(res)
        if (res.status === "success") {
        dispatch(IncidentDetailsRes(res));
        } else {
        dispatch(IncidentDetailsRes(res));
        }
        })
        .catch((e) => {
        });
    }
};


export function createActionRes(data) {
    return {
        type: ActionTypes.CREATEACTION,
        data
    }
};


