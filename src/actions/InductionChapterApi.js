import * as ActionTypes from '../constants/ActionTypes';
import { BaseUrl } from '../constants/api';
import {Alert, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';

export function InductionChapterRes(data) {
    return {
        type: ActionTypes.INDUCTIONCHAPTERS,
        data
    }
};
export function inductionChapterApi(data) {
  const value = {
    inductionId: data.id
  }
  return (dispatch) => {
  fetch(BaseUrl + `/listInductionChapterByInductionId`, {
  method: 'POST',
  headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization':data.token
            },
  body: JSON.stringify(value)
})
  .then((res) => res.json())
  .then(res => {
  dispatch(InductionChapterRes(res));
})
.catch((e) => {
                //  console.warn(e);
});
}
};

