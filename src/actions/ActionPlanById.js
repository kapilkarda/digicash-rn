import * as ActionTypes from '../constants/ActionTypes';
import { BaseUrl } from '../constants/api';
import {Alert, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';
export function actionPlanbyIdRes(data) {
    return {
        type: ActionTypes.ACTIONPLANBYID,
        data
    }
};

export function EditActionPlanRes(data) {
    return {
        type: ActionTypes.EDITACTIONPLAN,
        data
    }
};


export function actionplanbyIdAPI(data) {
    console.log("actionPlanbyIdRes",data)
    const sendData = {
        "id": data.id
    }
    return (dispatch) => {
        fetch(BaseUrl + `/actionPlanById`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization' : data.token

            },
            body: JSON.stringify(sendData)
        })
            .then((res) => res.json())
            .then(res => {
                console.log("actionPlanbyIdRes",res)
                if (res.status === "success") {
                    dispatch(EditActionPlanRes({}));
                    dispatch(actionPlanbyIdRes(res));
                   
                } else {
                   
                    dispatch(actionPlanbyIdRes(res));
                }
            })
            .catch((e) => {
                //  console.warn(e);
            });
    }
};

